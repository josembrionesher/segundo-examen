const express = require("express");
const router = express.Router();
const mysqlConnection = require("../database");

router.get("/verusuarios", (request, respuesta) => {
  mysqlConnection.query("SELECT * FROM elektra.usuarios;", (err, rows, fields) => {
    if (!err) {
      respuesta.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/verusuarios/:id", (request, respuesta) => {
  const id = request.params.id;
  console.log(id);

  mysqlConnection.query(
    "SELECT * FROM elektra.usuarios where idusuarios = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        respuesta.json(rows);
      } else {
        console.log(err);
      }
    }
  );
});



router.post("/guardarusuario", (request, respuesta) => {
    const valores = request.body;
    
     let idusuarios = valores.idusuarios
     let usuario= valores.usuario
     let password = valores.password
     let rol = valores.rol
     let email = valores.email
    

    mysqlConnection.query("INSERT INTO elektra.usuarios (idusuarios,usuario,password,rol,email) VALUES (?,?,?,?,?)",[idusuarios,usuario,password,rol,email],(err, rows, fields) => {
       if (!err) {
        
          respuesta.json({
            msg: "Guardado"
          });
        } else {
          console.log(err);
        }
       }
     );
  });



  router.delete("/eliminarusuario/:id", (request, respuesta) => {
    const id = request.params.id;
    console.log(id);
  
    mysqlConnection.query(
      "DELETE FROM elektra.usuarios where idusuarios = ?",
      [id],
      (err, rows, fields) => {
        if (!err) {
          respuesta.json({
            msg: "Eliminado"
          });
        } else {
          console.log(err);
        }
      }
    );
  });


  router.put("/actualizarusuario/:id", (request, respuesta) => {
    const id = request.params.id;
    const valores = request.body;
     let usuario= valores.usuario
     let password = valores.password
     let rol = valores.rol
     let email = valores.email
    


    mysqlConnection.query("UPDATE elektra.usuarios SET  usuario= ? ,password= ? ,rol= ? ,email = ?  where idusuarios = ?",[usuario, password, rol, email, id],(err, rows, fields) => {
       if (!err) {
        respuesta.json({
          msg: "Actualizado"
        });
        } else {
          console.log(err);
        }
       }
     );
  });

module.exports = router;