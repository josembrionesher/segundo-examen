const express = require("express");
const router = express.Router();
const mysqlConnection = require("../database");

router.get("/ver", (request, respuesta) => {
  mysqlConnection.query("SELECT * FROM elektra.productos;", (err, rows, fields) => {
    if (!err) {
      respuesta.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/ver/:id", (request, respuesta) => {
  const id = request.params.id;
  console.log(id);

  mysqlConnection.query(
    "SELECT * FROM elektra.productos where idproductos = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        respuesta.json(rows);
      } else {
        console.log(err);
      }
    }
  );
});



router.post("/guardar", (request, respuesta) => {
    const valores = request.body;
    
     let idproductos = valores.idproductos
     let nombreproducto= valores.nombreproducto
     let categoria = valores.categoria
     let stock = valores.stock
     let precio = valores.precio
    

    mysqlConnection.query("INSERT INTO elektra.productos (idproductos,nombrepro,categoria,stock,precio) VALUES (?,?,?,?,?)",
    [idproductos, nombreproducto, categoria, stock, precio],(err, rows, fields) => {
       if (!err) {
        
          respuesta.json({
            msg: "Guardado"
          });
        } else {
          console.log(err);
        }
       }
     );
  });



  router.delete("/eliminar/:id", (request, respuesta) => {
    const id = request.params.id;
    console.log(id);
  
    mysqlConnection.query(
      "DELETE FROM elektra.productos where idproductos = ?",
      [id],
      (err, rows, fields) => {
        if (!err) {
          respuesta.json({
            msg: "Eliminado"
          });
        } else {
          console.log(err);
        }
      }
    );
  });


  router.put("/actualizar/:id", (request, respuesta) => {
    const id = request.params.id;
    const valores = request.body;
    
 
     let nombreproducto= valores.nombreproducto
     let categoria = valores.categoria
     let stock = valores.stock
     let precio = valores.precio
    


    mysqlConnection.query("UPDATE elektra.productos SET  nombrepro= ? ,categoria = ? ,stock= ? ,precio= ? where idproductos = ?",[nombreproducto, categoria, stock, precio, id],(err, rows, fields) => {
       if (!err) {
        respuesta.json({
          msg: "Actualizado"
        });
        } else {
          console.log(err);
        }
       }
     );
  });

module.exports = router;