const express = require("express");
const router = express.Router();
const mysqlConnection = require("../database");

router.get("/vercompras", (request, respuesta) => {
  mysqlConnection.query("SELECT * FROM elektra.compra;", (err, rows, fields) => {
    if (!err) {
      respuesta.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/vercompras/:id", (request, respuesta) => {
  const id = request.params.id;
  console.log(id);

  mysqlConnection.query(
    "SELECT * FROM elektra.compra where id_compra = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        respuesta.json(rows);
      } else {
        console.log(err);
      }
    }
  );
});



router.post("/guardarcompras", (request, respuesta) => {
    const valores = request.body;
    
     let id_compra = valores.id_compra
     let cantidad= valores.cantidad
     let precio = valores.precio
     let id_usuario = valores.id_usuario
     let id_producto = valores.id_producto
    

    mysqlConnection.query("INSERT INTO elektra.compra (id_compra,cantidad, precio, id_usuario, id_producto) VALUES (?,?,?,?,?)",
    [id_compra,cantidad, precio, id_usuario, id_producto],(err, rows, fields) => {
       if (!err) {
        
          respuesta.json({
            msg: "Guardado"
          });
        } else {
          console.log(err);
        }
       }
     );
  });



  router.delete("/eliminarcompras/:id", (request, respuesta) => {
    const id = request.params.id;
    console.log(id);
  
    mysqlConnection.query(
      "DELETE FROM elektra.compra where id_compra = ?",
      [id],
      (err, rows, fields) => {
        if (!err) {
          respuesta.json({
            msg: "Eliminado"
          });
        } else {
          console.log(err);
        }
      }
    );
  });


  router.put("/actualizarcompras/:id", (request, respuesta) => {
    const id = request.params.id;
    const valores = request.body;
    
   
   let cantidad=valores.cantidad
   let precio= valores.precio
   let id_usuario = valores.id_usuario
    let id_producto = valores.id_producto
     

    mysqlConnection.query("UPDATE elektra.compra SET  cantidad = ? ,precio= ? ,id_usuario = ? ,id_producto= ? where id_compra= ? ",[cantidad,precio, id_usuario, id_producto, id],(err, rows, fields) => {
       if (!err) {
        respuesta.json({
          msg: "Actualizado"
        });
        } else {
          console.log(err);
        }
       }
     );
  });

module.exports = router;