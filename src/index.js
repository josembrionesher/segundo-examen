const servidor = require('express');
const servicio = servidor();

servicio.set('port', process.env.PORT || 3000);
//const host = '192.168.43.154';

servicio.use(servidor.json());

servicio.use(require('./router/permisos')) 

servicio.use(require('./router/productos'))
servicio.use(require('./router/usuarios'))
servicio.use(require('./router/compra'))

servicio.listen(servicio.get('port'),  ()=>{
    console.log('estoy vivo :>> ');
})